#coding:utf-8
import socket

host, port = ('localhost', 5568)
socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    socket.connect((host, port))
    print("Le client est connecté")

    data = "Bonjour serveur, je suis le client"
    data = data.encode("utf8")
    socket.sendall(data)

except ConnectionRefusedError:
    print("La connexion a echoué")
finally:
    socket.close()    
