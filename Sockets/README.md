# Socket
### connection en local entre deux terminaux, afin d'envoyer et recevoir un message

### sans thread :

 - serveur
 - client

### avec threads :

 - serveur avec class thread (permet d'écouter la connexion de plusieurs clients)
 - client 1
 - client 2
 - client 3
 - etc. 


