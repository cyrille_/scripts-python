import argparse
import os
import pathlib
from pathlib import Path

######################### arguments récupération des données :

parser = argparse.ArgumentParser()
parser.add_argument('message'           , help="Message à écrire dans le log")
parser.add_argument('chemin_fichier_log', help="Chemin e nom du fichier de journalisation")


args = parser.parse_args()

chemin_fichier = Path(args.chemin_fichier_log)
if chemin_fichier.is_file():
    
    with open(args.chemin_fichier_log, 'a+') as fichier_log:
        fichier_log.write(args.message+"\n")
        
else:
    
    with open(args.chemin_fichier_log, 'w') as fichier_log:
        fichier_log.write(args.message+"\n")
    