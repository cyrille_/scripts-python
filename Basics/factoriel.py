'''


fact(3) = 1*2*3 = 6
fact(4) = 1*2*3*4 = 24
fact(5) = 1*2*3*4*5 = 120

'''

def factoriel(nombre):
    facteur = 1

    for nombre_incremente in range(2, nombre + 1):
        facteur = facteur * nombre_incremente

    print("Le factoriel de :",  nombre,  "est : ", facteur)

factoriel(8)
