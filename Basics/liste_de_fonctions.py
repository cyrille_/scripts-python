'''C’est la méthode la plus simple. Nous parcourons simplement la liste
en utilisant la boucle for et appliquons la fonction requise à chaque
élément individuellement. Nous stockons le résultat dans une variable distincte,
puis ajoutons cette variable à une nouvelle liste.

Dans le code suivant, nous appliquons une fonction définie par l’utilisateur,
qui multiplie un nombre à 10.'''

def fn(a):
    return 10 * a

lst = [1, 2, 3, 4]
ans = []

for i in lst:
    x = fn(i)
    ans.append(x)
print(ans)
