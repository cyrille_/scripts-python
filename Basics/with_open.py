
#########################################################################
# Nom du projet : Journalisation des logs
# Nom du développeur : Cyrille
# Version : 1.0
# Date de création : 30/09/2021
# Date de modification : 30/09/2021
# Version de Python : 3.9.1
# Licence : OSB
# Bibliothèques annexes : 
# Documentation : 
#########################################################################

argument_d_dossier = "une ligne pour voir si ça fonctionne"
journal = "journal.log"

# Créer un fichier de logs, où seront stockées les données
with open(journal, "w"):
   print("fichier créé")

# On écrit dans le fichier les informations nécessaires :
with open(journal, 'a+') as fichierEcrit:
   fichierEcrit.write(argument_d_dossier)

# Nous allons lire le fichier configuration.conf
with open(journal, 'r') as fichierLu:
   print(fichierLu.readlines())
