from collections import Counter
import re

texte = 'salut; bernard, et salut !renaud'

mots = re.findall(r'\w+', texte)
print(Counter(mots))


#OU alors

import re

texte = 'salut; bernard, et salut !renaud'
div = re.split('[^a-zA-Z]', texte)
dico = {}

for mot in div:
    if mot != "":
        if mot in dico:
            dico[mot] += 1
        else:
            dico[mot] = 1
print("Counter",dico)