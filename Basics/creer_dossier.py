
import os
# ----------------------------------------------------------------------------------------
def creerDossier(nom_dossier):
    os.mkdir(nom_dossier)
    os.mkdir(nom_dossier+"/source")
    os.mkdir(nom_dossier+"/lib")
    os.mkdir(nom_dossier+"/file")
    fd = os.open(nom_dossier+"/texte.txt", os.O_CREAT)
    os.close(fd) 
# ----------------------------------------------------------------------------------------
print ("Vous vous trouvez ", os.getcwd())
while True :
    reponse = input("Voulez-vous créer un dossier ? (o/n)")
    if reponse.upper() == "O":
        nom_dossier = input("Quel est son nom ?")
        try:
            creerDossier(nom_dossier)
            print("Le dossier %s a bien été créé" %(nom_dossier))
            quit()
        except FileExistsError :
            rep_del = input("Le dossier existe déja voulez-vous le supprimer ?")
            if rep_del.upper() == "O":
                if len(os.listdir(nom_dossier) ) == 0:
                    os.rmdir(nom_dossier)
                else:
                    print("Le dossier n'est pas vide")
                    print(os.listdir(nom_dossier))
                    quit()
            else:
                quit()
    elif  reponse.upper() == "N":
        quit()
    else:
        print("Je n'ai pas compris votre réponse ")