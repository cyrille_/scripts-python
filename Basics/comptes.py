import pathlib
import os
import shutil
#import journalisation
from pathlib import Path
import subprocess
from send2trash import send2trash
from datetime import datetime
import time
import zipfile
#import oschmod

rep_racine          = "C:\Python_Projets\Automation\devops1\Creation_Compte\\"
rep_archive         = "C:\Python_Projets\Automation\devops1\Creation_Compte\Archives\\"
prog_journal        = "C:\Python_Projets\Automation\devops1\Creation_Compte\Log.py"
rep_compte          = rep_racine+"Comptes\\"
rep_modele_public   = rep_racine+"Modeles_Fichiers\\"
liste_rep_compte    = ["Privé","Public"]
prefixe_compte_user = "Compte_"
nom_compte          = ""
message_journal     = ""
fichier_log         = ""

try:
    #Demande à l'utilisateur le nom du compte à créer
    nom = input("Veuillez entrer un nom d'utilisateur : ")
    
    #Positionnement dans le repertoire contenant tous les comptes
    os.chdir(rep_compte)
    
    #Récupération de la liste des comptes existants
    chemin = os.listdir()
    
    #Initialisation de la liste des comptes alternatifs
    liste_propo = []

    #Le nom du compte est déjà présent
    if prefixe_compte_user+nom in chemin:
        i=1
        #Boucle de création des propositions de nom de compte
        while len(liste_propo)<=4:
            # Si le nom du compte proposé n'est pas existant
            if prefixe_compte_user+nom+str(i) not in chemin:
                # Ajout du compte dans la liste des propositions
                liste_propo.append(prefixe_compte_user+nom+str(i))
            i+=1

        y=1
        # Boucle sur l'ensemble des propositions des comptes
        for index, item in enumerate(liste_propo) :
            # Affiche les propositions à l'utilisateur
            print(str(y)," Proposition :"+item)
            y+=1
        # Demande à l'utilisateur de choisir une alternatives
        choix_prop = input("Veuillez faire un choix en tapant un chiffre de 1 à 5 : ")

        #Contrôle que le choix est bien dans la fourchette de 1 à 5
        if int(choix_prop) <=0  or int(choix_prop) >= 6  :
            quit()
        else : 
            #Récupère le nom du compte choisie
            nom = liste_propo[int(choix_prop)-1]
            nom_compte = nom 
    else:
        nom_compte = prefixe_compte_user+nom
    os.mkdir(nom_compte)
    
    # Variable pour dater le nom du fichier de journalisation
    date_creation = time.strftime("%Y-%m-%d__%H-%M")
    # Variable pour le nom du fichier de journalisation
    fichier_log = f"journal_{nom_compte}_{date_creation}.log"
    
    # Journalisation du succès de la création du compte
    subprocess.call(["python",prog_journal,f"### Création du compte {nom_compte} le {date_creation} ##########",rep_racine+fichier_log], shell=True)
            
    os.chdir(nom_compte)

    try:
        nom_rep_tmp =""
        for rep in liste_rep_compte:
            nom_rep_tmp = rep
            if rep=="Public":
                path1 = Path(rep_modele_public)
                try:
                    path3 = shutil.copytree(path1, rep)
                    
                    # Journalisation de la réussite de la création du sous répertoire du compte
                    subprocess.call(["python",prog_journal,f"### Création du répertoire {rep}",rep_racine+fichier_log], shell=True)  
                    # Journalisation de la réussite de la création des sous répertoire du compte Public
                    subprocess.call(["python",prog_journal,f"### Création de l'arborescence du modèle {rep_modele_public}",rep_racine+fichier_log], shell=True)            
                    
                except Exception as E:
                    # Journalisation de l'echec de la méthode copytree                    
                    subprocess.call(["python",prog_journal,f"### Impossible de copier les fichiers du répertoire modèle {rep_modele_public} => {E}",rep_racine+fichier_log], shell=True)
                    quit()                    
            else:
                os.mkdir(rep)
                # Journalisation de la réussite de la création du sous répertoire du compte
                subprocess.call(["python",prog_journal,f"### Création du répertoire {rep}",rep_racine+fichier_log], shell=True)  
                
                
        # Demande s'il faut archiver le compte               
        reponse_archive = input("Souhaitez-vous archiver le dossier (o/n) ?").upper()
        
        if reponse_archive=="O":
            os.chdir(rep_compte)
            try:
                shutil.make_archive(rep_compte+nom_compte,'zip',rep_compte,nom_compte)
                #Journalisation de la réussite de la création de l'archive
                subprocess.call(["python",prog_journal,f"### Création de l'archive {nom_compte}.zip",rep_racine+fichier_log], shell=True)
            except Exception as E:
                # Journalisation de l'echec de la méthode copytree                    
                subprocess.call(["python",prog_journal,f"### Impossible de consitituer l'archive=> {E}",rep_racine+fichier_log], shell=True)
                quit()                    
            
            try:
                #Récupération du fichier ZIP
                with zipfile.ZipFile(rep_compte+"\\"+nom_compte+".zip","r") as doczip:
                    #doczip = zipfile.
                    # Parcour de l'ensemble des fichiers contenu dans l'archive
                    for fichier_zip in doczip.filelist:
                        subprocess.call(["python",prog_journal,f"### Contenu de l'archive {nom_compte}.zip => {fichier_zip.filename}",rep_racine+fichier_log], shell=True)
            except Exception as E:
                # Journalisation de l'echec de la méthode copytree                    
                subprocess.call(["python",prog_journal,f"### Impossible de lister l'archive=> {E}",rep_racine+fichier_log], shell=True)
                quit()
            try:
                shutil.move(rep_compte+nom_compte+".zip",rep_archive+nom_compte+".zip")
            
                #Journalisation de la réussite du déplacement de l'archive
                subprocess.call(["python",prog_journal,f"### Déplacement de l'archive {nom_compte}.zip sous {rep_archive}",rep_racine+fichier_log], shell=True)
            except Exception as E:
                # Journalisation de l'echec de la méthode copytree                    
                subprocess.call(["python",prog_journal,f"### Impossible de déplacer l'archive=> {E}",rep_racine+fichier_log], shell=True)
                quit()
        # Demande s'il faut supprimer le compte
        reponse_suppression = input("Souhaitez-vous supprimer le dossier (o/n) ?").upper()
        
        if reponse_suppression=="O":
            
            #Se positionner dans le répertoire du compte
            os.chdir(rep_compte)
            
            # Mise à la poubelle du compte
            send2trash(nom_compte)
            
            # JOurnalisation de la réussite de la suppression du compte
            subprocess.call(["python",prog_journal,f"### Suppression du Compte {nom_compte} ",rep_racine+fichier_log], shell=True)
    except Exception as E:
        # Log de l'echec
        subprocess.call(["python",prog_journal,f"Impossible de créer le dossier {nom_rep_tmp} : {E} ",rep_racine+fichier_log], shell=True)
        
        
except Exception as E:
    # Log de l'echec
    subprocess.call(["python",prog_journal,f"Impossible de créer le compte {nom_compte} : {E}",rep_racine+fichier_log], shell=True)
    