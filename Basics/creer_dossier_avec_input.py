import os
chemin_actuel = os.getcwd()
liste_rep = ["src","lib","file"]
liste_fic = ["texte.txt"]
print(f"Vous êtes actuellement {chemin_actuel}")
 
rep = input("Souhaitez-vous créer un nouveau dossier (o/n)").upper()
if rep=="O":
    rep2= input("Quel est son nom ? :")
    for lettre in rep2:
        if lettre == "_":
            print("Le nom de dossier ne doit pas contenir d'espace")
            quit()
    try:
        for item in liste_rep:
            os.makedirs(rep2+"\\"+item)
        open(rep2+"\\texte.txt","w+")

    except FileExistsError:
        
        print("Ce dossier existe déjà")
        
        doss = os.listdir(rep2)
        if len(doss)>0:
            
            rep3 = input (f"Souhaitez-vous remplacer le dossier {rep2} ? (o/n)").upper()
            if rep3=="O":
                for item in liste_rep:
                    os.removedirs(rep2+"\\"+item)
                os.remove(rep2+"\\texte.txt")
                for item in liste_rep:
                    os.makedirs(rep2+"\\"+item)
                open(rep2+"\\texte.txt","w+")
            else:
                print("Le dossier n'est pas vide :")
                for index, item in enumerate(doss) :
                    print("|__ :",item)