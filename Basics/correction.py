# coding: utf-8
###############################################
# * Nom du projet : automatisation de création de compte utilisateur
# * Nom du développeur : Jean-Lou Le Bars (jllebars@cefim.eu), Thomas Cherrier,
# Version : 2.56743.987234
# Date de création : 01/06/2021
# * Date de modification : 01/10/2021
# * Version de Python : 3.8.5
# Licence : CEFIM
# Bibliothèques annexes : send2trash, zipfile
# Documentation : /cefim/documentation/python/automatisation.md
# ? Historique des modifications :
# ? - utilisation de la méthode copy2
# ! TODO :
# ! - Proposer une série de nom en cas de doublon
# ! - Gérer les droits, quel que soit l’OS
# ! - Protéger la suppression avec un ID User
# ! - Déplacer les fichiers log et d’archives dans un dossier spécifique
###############################################
'''
@author :
@param :
@date :
'''
import time
from pathlib import Path
import shutil
import os
import send2trash
import zipfile
# La racine de votre serveur
print(f"La racine est : {Path.home()}")
# Parcours des fichiers à la racine
chemin_complet = Path(Path.cwd())
print(f"Vous êtes actuellement situé à : {chemin_complet}.")
# Variable pour le nom du salarié
nom_salarie = input("Veuillez saisir le nom du nouveau salarié : ")
# Variable pour dater le nom du fichier de journalisation
datation = time.strftime("%Y-%m-%d__%H-%M")
fichier_log = Path("journalisation_" + nom_salarie + "_" + datation + ".log")
# Pour le pas faire d’erreur de typage dans l’utilisation d’une variable :
# print(type(fichier_log))
fichier_log.open("w").write("### DOSSIER de " + nom_salarie.upper() + " créé le " + datation + ".\n")
fichier_log.open("a").write("===========================================\n")
fichier_log.open("a").write("### ==> Emplacement du fichier de journalisation : " + str(fichier_log.cwd())+ ".\n")
fichier_log.open("a").write("===========================================\n")
dossier_compte = str(Path.cwd() / "compte/")
# La variable folder est un dictionnaire comprenant en clé la nature du dossier et en valeur le chemin du dossier.
folders = {"NOM_SALARIE" : dossier_compte + "/" + nom_salarie,
          "PUBLIC" : dossier_compte + "/" + nom_salarie + "/public",
          "RH" : dossier_compte + "/" + nom_salarie + "/public/rh",
          "CONGES" : dossier_compte + "/" + nom_salarie + "/public/rh/conges",
          "MISSION" : dossier_compte + "/" + nom_salarie + "/public/rh/fiche_mission",
          "DAF" : dossier_compte + "/" + nom_salarie + "/public/daf",
          "FRAIS" : dossier_compte + "/" + nom_salarie + "/public/daf/note_frais",
          "PAIE" : dossier_compte + "/" + nom_salarie + "/public/daf/fiche_paie",
          "PRIVE" : dossier_compte + "/" + nom_salarie + "/prive"}
# Nous parcourons chaque clé de folder et prenons sa valeur pour en créer un chemin.
for key in folders:
    try :
        Path(folders[key]).mkdir(parents=True, exist_ok=False)
        fichier_log.open("a").write("### ==> Création des dossiers : " + str(folders[key]).split("/")[-1] + " ----------> OK. \n")
    except Exception as e :
        print(f"Un répertoire portant le nom ‘{nom_salarie}’ existe déjà.")
        fichier_log.write_text("### ==> Création des dossiers : KO")
        fichier_log.write_text(e)
        quit()
for racine, dossiers, fichiers in os.walk("Modeles_Fichiers"):
    for fichier in fichiers:
        nom_fichier, extension = os.path.splitext(fichier)
        theme_fichier = nom_fichier.split("_")
        chemin_source = os.path.join(racine,fichier)
        if theme_fichier[1] == "mission":
            chemin_cible = folders["MISSION"]
            shutil.copy2(chemin_source, chemin_cible)
            fichier_log.open("a").write("===========================================\n")
            fichier_log.open("a").write(f"### ==> Déplacement des modèles de fichiers {fichier} : OK. \n")
        elif theme_fichier[1] == "conges":
            chemin_cible = folders["CONGES"]
            shutil.copy2(chemin_source, chemin_cible)
            fichier_log.open("a").write(f"### ==> Déplacement des modèles de fichiers {fichier} : OK. \n")
        elif theme_fichier[1] == 'frais':
            chemin_cible = folders["FRAIS"]
            shutil.copy2(chemin_source, chemin_cible)
            fichier_log.open("a").write(f"### ==> Déplacement des modèles de fichiers {fichier} : OK. \n")
        else :
            fichier_log.open("a").write("===========================================\n")
            fichier_log.open("a").write(f"### ==> Il y a eu un problème lors de la création des dossiers. \n")
# Nous allons créer une archive de tout ce qui a été créé
archiver_Dossier = input(f"Voulez-vous archiver le dossier de {nom_salarie} (O/N) : ")
if archiver_Dossier.lower() == "o":
    archive_zip = shutil.make_archive(nom_salarie + "_" + datation, "zip", dossier_compte + "/" + nom_salarie)
# Nous allons lire cette archive puis l’écrire dans le fichier log.
    contenu_zip = zipfile.ZipFile(archive_zip)
    fichier_log.open("a").write("===========================================\n")
    fichier_log.open("a").write(f"### ==> Contenu du fichier archivé {archive_zip} : \n")
    for item in contenu_zip.namelist():
        fichier_log.open("a").write(f"### ==> {item} \n")
else:
    fichier_log.open("a").write("===========================================\n")
    fichier_log.open("a").write(f"### ==> Contenu du fichier non archivé.\n")
supprimer_dossier_salarie = input(f"Voulez-vous supprimer le dossier du salarié {nom_salarie} (O/N) : ")
if supprimer_dossier_salarie.lower() == "o":
    send2trash.send2trash(dossier_compte + "/" + nom_salarie)
    fichier_log.open("a").write("===========================================\n")
    fichier_log.open("a").write(f"### ==> Le dossier de {nom_salarie} a été supprimé. \n")
else:
    fichier_log.open("a").write("===========================================\n")
    fichier_log.open("a").write(f"### ==> Le dossier de {nom_salarie} n’a pas été supprimé. \n")
# Une fois tout le processus fini, nous renommons le fichier de log.
fichier_log_fini = Path("journalisation_" + nom_salarie + "_" + datation + "_creation.log")
fichier_log.rename(fichier_log_fini)