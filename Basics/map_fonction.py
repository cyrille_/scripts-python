'''La fonction map() est utilisée pour appliquer une fonction
à tous les éléments d’un objet itérable spécifique comme une liste,
un tuple, etc. Il renvoie un objet de type map qui peut être converti
en liste par la suite à l’aide de la fonction list().

Par exemple,'''


def fn(a):
    return 10 * a

lst = [1, 2, 3, 4]

ans = list(map(fn, lst))
print(ans)