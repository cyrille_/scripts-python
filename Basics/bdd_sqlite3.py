import sqlite3 as sql

with sql.connect("test.db") as conn:
    c = conn.cursor()
    c.execute(
        '''
            CREATE TABLE IF NOT EXISTS article (
                id int,
                title VARCHAR(50),
                content text            
            )
        '''
        )
    
    # c.execute(" INSERT INTO article (id,title,content) values (1,'Formation Python','blablablabla') ")

    # conn.commit()

    # c.execute("DELETE FROM article WHERE id = 2")

    c.execute("UPDATE article SET title='Formation Devops' WHERE id = 1 ")

    stmt = c.execute("SELECT * FROM article")

    for id,title,content in stmt:
        print(id,title,content)
    