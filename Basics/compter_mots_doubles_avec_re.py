import re

texte = "Salut tout le monde, c'est un texte pour tester le code... Salut tout le monde, c'est un texte pour tester le code..."
div = re.split('[^a-zA-Z]', texte)
dico = {}

for mot in div:
    if mot != "":
        if mot in dico:
            dico[mot] += 1
        else:
            dico[mot] = 1
print(dico)