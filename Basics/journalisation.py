#########################################################################
# Nom du projet : Journalisation des logs
# Nom du développeur : Cyrille
# Version : 1.0
# Date de création : 30/09/2021
# Date de modification : 30/09/2021
# Version de Python : 3.9.1
# Licence : OSB
# Bibliothèques annexes : 
# Documentation : 
#########################################################################
import os
from datetime import datetime

date = str(datetime.now())
m_date = "le dossier de "
m_date_ = "a ete cree le : "
journal = "journal.log"
m_archive = "Chemin de l'archive : "
m_dossier = "Creation des dossiers :  "
m_modele = "Deplacement des modeles de fichiers : "
delimiteur =   "========================================================"
delimitateur = "====================  DEBUT DU LOG   ==================="

######################### Variables pour récupérer les données logs :

nom_modele = ""
nom_dossier = ""
nom_archive = ""
nom_compte = ""
nom_erreur_deplacement_fichiers = ""
nom_erreur_creation_dossier = ""
nom_erreur_creation_de_l_archive = ""
nom_erreur_creation_de_compte = ""

######################### arguments récupération des données :

""" parser = argparse.ArgumentParser()
parser.add_argument('nom_modele', action="store", default='fort.13', type=str)
parser.add_argument('nom_dossier', action="store", default='fort.13', type=str)
parser.add_argument('nom_archive', action="store", default='fort.13', type=str)
parser.add_argument('nom_compte', action="store", default='fort.13', type=str)
parser.add_argument('nom_erreur_deplacement_fichiers', action="store", default='fort.13', type=str)
parser.add_argument('nom_erreur_creation_dossier', action="store", default='fort.13', type=str)
parser.add_argument('nom_erreur_creation_de_l_archive', action="store", default='fort.13', type=str)
parser.add_argument('nom_erreur_creation_de_compte', action="store", default='fort.13', type=str)

args = parser.parse_args()

nom_modele = args.nom_modele
nom_dossier = nom_modele = args.nom_dossier
nom_archive = nom_modele = args.nom_archive
nom_compte = nom_modele = args.nom_compte
nom_erreur_deplacement_fichiers = nom_modele = args.nom_erreur_deplacement_fichiers
nom_erreur_creation_dossier = nom_modele = args.nom_erreur_creation_dossier
nom_erreur_creation_de_l_archive = nom_modele = args.nom_erreur_creation_de_l_archive
nom_erreur_creation_de_compte = nom_modele = args.nom_erreur_creation_de_compte """

######################### Création d'un fichier log

with open(journal, 'a+') as ins:
   ins.write("\n")  
with open(journal, "a+") as fichier:
	print("")

######################### Délimiteur

with open(journal, 'a+') as ins:
   ins.write("\n")
with open(journal, 'a+') as ins:
   ins.write(delimiteur)
with open(journal, 'a+') as ins:
   ins.write("\n")
######################### Délimiteur

with open(journal, 'a+') as ins:
   ins.write(delimitateur)
with open(journal, 'a+') as ins:
   ins.write("\n")
with open(journal, 'a+') as ins:
   ins.write(delimiteur)
with open(journal, 'a+') as ins:
   ins.write("\n")

######################## Insertion de la date de création du fichier

with open(journal, 'a+') as ins:
   ins.write(m_date)
with open(journal, 'a+') as ins:
   ins.write(nom_compte)
with open(journal, 'a+') as ins:
   ins.write(m_date_)
with open(journal, 'a+') as ins:
   ins.write(date)

######################### Délimiteur

with open(journal, 'a+') as ins:
   ins.write("\n")
with open(journal, 'a+') as ins:
   ins.write(delimiteur)
with open(journal, 'a+') as ins:
   ins.write("\n")

######################### Nom du dossier

with open(journal, 'a+') as ins:
   ins.write(m_dossier)
with open(journal, 'a+') as ins:
   ins.write("\n")
with open(journal, 'a+') as ins:
   ins.write(nom_dossier)
with open(journal, 'a+') as ins:
  ins.write("\n")

######################### Délimiteur

with open(journal, 'a+') as ins:
   ins.write("\n")
with open(journal, 'a+') as ins:
   ins.write(delimiteur)
with open(journal, 'a+') as ins:
   ins.write("\n")

######################### Nom du fichier déplacé

with open(journal, 'a+') as ins:
   ins.write(m_modele)
with open(journal, 'a+') as ins:
   ins.write("\n")
with open(journal, 'a+') as ins:
   ins.write(nom_modele)
with open(journal, 'a+') as ins:
  ins.write("\n")

######################### Délimiteur

with open(journal, 'a+') as ins:
   ins.write("\n")
with open(journal, 'a+') as ins:
   ins.write(delimiteur)
with open(journal, 'a+') as ins:
   ins.write("\n")

######################### Nom de l'archive créée

with open(journal, 'a+') as ins:
   ins.write(m_archive)
with open(journal, 'a+') as ins:
   ins.write("\n")
with open(journal, 'a+') as ins:
   ins.write(nom_archive)
with open(journal, 'a+') as ins:
  ins.write("\n")

with open(journal, "r") as fs:
   lignes = [ligne.rstrip() for ligne in fs.readlines()]
lignes = lignes[-30:]

for ligne in lignes:
  print(ligne)

######################### Liste des erreurs

with open(journal, 'a+') as ins:
   ins.write(nom_erreur_deplacement_fichiers)
with open(journal, 'a+') as ins:
   ins.write(nom_erreur_creation_dossier)
with open(journal, 'a+') as ins:
   ins.write(nom_erreur_creation_de_l_archive)
with open(journal, 'a+') as ins:
  ins.write(nom_erreur_creation_de_compte)