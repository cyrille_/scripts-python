def mult(a,b):
    return a*b

print(mult(2,5)) # ou c = mult(2,5)

def ajouter(a,b):
    print(a,"+",b,"=",a+b)

ajouter(1,5)

'''

adresse mémoire de la fonction 

print(mult)
z = mult
print(z)

'''
print(mult)
z = mult
print(z)

def appeler_une_fontion(f,a,b):
    f(a,b)

appeler_une_fontion(ajouter,5,2)