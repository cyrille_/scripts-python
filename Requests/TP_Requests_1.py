import re
from collections import Counter
import csv
fichier = "logs"
#expression = "\d{1,4}\-\d{1,2}\-\d{1,2}"
expression = "\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"
#expression = "IP"
# expression = "(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})”
with open(fichier) as f:
    contenu = f.read()
    print(contenu)
    liste_des_ip = re.findall(expression, contenu)
    print(liste_des_ip)
    print(Counter(liste_des_ip))
    with open("ip_file.csv", "w") as f:
        ecrire_dans_csv = csv.writer(f)
        ecrire_dans_csv.writerow(["IP", "nb de fois"])
        for cle, valeur in Counter(liste_des_ip).items() :
            #print(f”L’IP {cle} est contenue {valeur} fois.“)
            ecrire_dans_csv.writerow([cle, valeur])