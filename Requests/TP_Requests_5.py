import requests

#Appel de la méthode GET de l'API github pour obtenir les bibliothèques de requêtes dans le langage Python
reponse_get = requests.get('https://api.github.com/search/repositories',params={'q': 'requests+language:python'})

#Traitement de la réponse de la méthode GET
if reponse_get.status_code == 200:
    print("Gagné!")
    print("Vous obtenez :")
    
    #Récupération du JSON de la requête
    ma_reponse_json = reponse_get.json()
    
    #Affichage des nom des dépôts et de leurs desciptions
    for item in ma_reponse_json['items'] :
        print(f'Repository name: {item["name"]} description : {item["description"]}')
elif reponse_get.status_code == 404:
    print("Non trouvé :-(")
