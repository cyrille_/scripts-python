import requests
import json
#Appel de la méthode GET de l'API github
reponse_get = requests.get('https://api.github.com')

#Traitement de la réponse de la méthode GET
if reponse_get.status_code == 200:
    print("Gagné!")
    print("Vous obtenez :")
    ma_reponse_text = reponse_get.text
    ma_reponse_json = json.loads(ma_reponse_text)
    print(ma_reponse_json)
elif reponse_get.status_code == 404:
    print("Non trouvé :-(")
