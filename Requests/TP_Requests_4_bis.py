import requests

#Appel de la méthode GET de l'API github
reponse_get = requests.get('https://api.github.com')

#Traitement de la réponse de la méthode GET
if reponse_get.status_code == 200:
    print("Gagné!")
    print("Vous obtenez :")
    
    #Récupération du JSON de la reqête
    ma_reponse_json = reponse_get.json()
    print(ma_reponse_json)
elif reponse_get.status_code == 404:
    print("Non trouvé :-(")
