# coding: utf-8
###############################################
# * Nom du projet : automatisation de création de compte utilisateur
# * Nom du développeur : 
# Version : 2.567
# Date de création : 01/10/2021
# * Date de modification : 02/10/2021
# * Version de Python : 3.8.5
# Licence : OSB
# Bibliothèques annexes : send2trash
# Documentation : automatisation.md
# ? Historique des modifications :

# le 2 octobre : 

# -Changement code pour fonctionnement sous système Linux
# -Création automatique des dossiers

# ! TODO :
# ! - Proposer une série de nom en cas de doublon
# ! - Gérer les droits, quel que soit l’OS
# ! - Protéger la suppression avec un ID User
# ! - Déplacer les fichiers log et d’archives dans un dossier spécifique
###############################################

#'''
#@author :
#@param :
#@date :
#'''


import pathlib
import os
import shutil
from pathlib import Path
import subprocess
from send2trash import send2trash
from datetime import datetime
from pathlib import Path

rep_racine = Path("/home/cyrille/Bureau/opt/logiciel")
rep_archive = Path("/home/cyrille/Bureau/opt/logiciel/archive")
rep_compte = Path("/home/cyrille/Bureau/opt/logiciel/Comptes")
rep_modele_public = Path("/home/cyrille/Bureau/opt/logiciel/Modeles_Fichiers")
liste_rep_compte    = ["Public","Privé"]
prefixe_compte_user = "Compte_"
nom_compte          = ""
delimiteur          =   "========================================================"
delimitateur        = "====================  DEBUT DU LOG   ==================="

#Création des dossiers :

path_a = Path("/home/cyrille/Bureau/opt/logiciel")
path_b = Path("/home/cyrille/Bureau/opt/logiciel/archive")
path_c = Path("/home/cyrille/Bureau/opt/logiciel/Comptes")
path_d = Path("/home/cyrille/Bureau/opt/logiciel/Modeles_Fichiers")

path_a.mkdir(parents=True, exist_ok=True)
path_b.mkdir(parents=True, exist_ok=True)
path_c.mkdir(parents=True, exist_ok=True)
path_d.mkdir(parents=True, exist_ok=True)



try:
    #Demande à l'utilisateur le nom du compte à créer
    nom = input("Veuillez entrer un nom d'utilisateur : ")
    
    #Positionnement dans le repertoire contenant tous les comptes
    os.chdir(rep_compte)
    
    #Récupération de la liste des comptes existants
    chemin = os.listdir()
    
    #Initialisation de la liste des comptes alternatifs
    liste_propo = []
    #Le nom du compte est déjà présent
    if prefixe_compte_user+nom in chemin:
        i=1
        #Boucle de création des propositions de nom de compte
        while len(liste_propo)<=4:
            # Si le nom du compte proposé n'est pas existant
            if prefixe_compte_user+nom+str(i) not in chemin:
                # Ajout du compte dans la liste des propositions
                liste_propo.append(prefixe_compte_user+nom+str(i))
            i+=1
        y=1
        # Boucle sur l'ensemble des propositions des comptes
        for index, item in enumerate(liste_propo) :
            # Affiche les propositions à l'utilisateur
            print(str(y)," Proposition :"+item)
            y+=1
        # Demande à l'utilisateur de choisir une alternatives
        choix_prop = input("Veuillez faire un choix en tapant un chiffre de 1 à 5 : ")
        #Contrôle que le choix est bien dans la fourchette de 1 à 5
        if int(choix_prop) <=0  or int(choix_prop) >= 6  :
            quit()
        else : 
            #Récupère le nom du compte choisie
            nom = liste_propo[int(choix_prop)-1]
            nom_compte = nom 
    else:
        nom_compte = prefixe_compte_user+nom
    os.mkdir(nom_compte)
    ######################### Création d'un fichier log
    date_creation = str(datetime.now())
    with open("/home/cyrille/Bureau/opt/logiciel/journal.log", 'a+') as ins:
        ins.write("========================================================/n")
        ins.write(f"Création du compte {nom_compte} le {date_creation} /n")  
    
    #subprocess.call(f"python {rep_journal} '' '' '' {nom_compte} '' '' '' ''", shell=True) 
        
    os.chdir(nom_compte)
    try:
        nom_rep_tmp =""
        for rep in liste_rep_compte:
            nom_rep_tmp = rep
            if rep=="Public":
                path1 = Path(rep_modele_public)
                try:
                    path3 = shutil.copytree(path1, rep)
                    # Log de la réussite
                    with open("/home/cyrille/Bureau/opt/logiciel/journal.log", 'a+') as ins:
                        ins.write(f"Création du répertoire {rep} /n")
                        
                    #subprocess.call(f"python {rep_journal} '' '' {rep} '' '' '' '' ''", shell=True)
                    
                except:
                    # Log de l'echec
                    with open("/home/cyrille/Bureau/opt/logiciel/journal.log", 'a+') as ins:
                        ins.write("===================ERREUR===============================/n")
                        ins.write(f"Impossible de copier les fichiers modèles {rep_modele_public} /n")
                    
            else:
                # Log de la réussite
                with open("/home/cyrille/Bureau/opt/logiciel/journal.log", 'a+') as ins:
                    ins.write(f"Création du répertoire {rep} /n")
                os.mkdir(rep)
        # Log de la réussite
        with open("/home/cyrille/Bureau/opt/logiciel/journal.log", 'a+') as ins:
            ins.write(f"Création du dossier {rep_modele_public} dans le dossier Public /n")
        
        reponse_archive = input("Souhaitez-vous archiver le dossier (o/n) ?").upper()
        print(os.getcwd())
        if reponse_archive=="O":
            #os.chdir(rep_compte)
            shutil.make_archive(nom_compte,'zip',rep_archive,rep_compte)
            shutil.move(rep_compte+"/"+nom_compte+"/"+nom_compte+".zip",rep_archive+"/"+nom_compte+".zip")
            # Log de l'echec
            with open("/home/cyrille/Bureau/opt/logiciel/journal.log", 'a+') as ins:
                ins.write(f"Création de l'archive {nom_compte} /n")
        
        reponse_suppression = input("Souhaitez-vous supprimer le dossier (o/n) ?").upper()
        if reponse_suppression=="O":
            os.chdir(rep_compte)
            send2trash(nom_compte)
            with open("/home/cyrille/Bureau/opt/logiciel/journal.log", 'a+') as ins:
                ins.write(f"Suppression du compte {nom_compte} /n")
    except Exception as E:
        # Log de l'echec
        with open("/home/cyrille/Bureau/opt/logiciel/journal.log", 'a+') as ins:
            ins.write("===================ERREUR===============================/n")
            ins.write(f"Impossible de créer le dossier {nom_rep_tmp} /n")
        
except Exception as E:
    # Log de l'echec
    with open("/home/cyrille/Bureau/opt/logiciel/journal.log", 'a+') as ins:
        ins.write("===================ERREUR===============================/n")
        ins.write(f"Impossible de créer le compte {nom_compte} /n")
