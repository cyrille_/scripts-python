## Scripting Python

#### Basic
  - chemin.py
  - chercher_regex_redater.py
  - compter_mots_doubles_avec_re.py
  - compter_mots_doublons.py
  - comptes.py
  - contacts.txt
  - correction.py
  - course_pas_termine.py
  - courses.py
  - courses.txt
  - creer_dossier_avec_input.py
  - creer_dossier.py
  - decompte_et_affichage_doublons.py
  - demo_fonctions_pair_impair.py
  - enumerate_mots-et-index.py
  - facteur_avec_arguments_en_test.py
  - factoriel_avec_input.py
  - factoriel.py
  - fonction_afficher.py
  - fonction_param.py
  - indentifier_systeme.py
  - index_identifie.py
  - journalisation.py
  - json.py
  - liste_de_fonctions.py
  - log.error
  - Log.py
  - manipulation_piles.py
  - map_fonction.py
  - nombre_occurences.py
  - paragraphe.txt
  - parcourir_lettres.py
  - test.db
  - test_exceptions.py
  - test_if.py
  - test_piles.py
  - verifie_si_existe.py
  - with_open.py
  - zipfile.py

#### Paramiko library

  - bdd_sqlite3
  - code
  - correction_tp_email
  - correction_tp_json_sockets
  - correction_tp_POO
  - correction_tp_ssh
  - correction_tp_ssh_send_mail
  - demo_packages_et_modules
  - demo_sockets
  - espace_travail
  - formation_python
  - heritage_poo_tests_unitaires
  - manipuler_les_fichiers_et_gérer_les_exceptions
  - paramiko_sftp
  - sftp_ftp

#### Requests library

  - Requests

#### Sockets

  - connexion avec et sans thread

#### Comptes

  - Création automatisée comptes

  
#### Ressources sur python

  - [FormationVideo](https://youtube.com/playlist?list=PLrSOXFDHBtfHg8fWBd7sKPxEmahwyVBkC)
  - [Mooc complet France Université Numérique](https://gitlab.com/cyrille_/scripts-python/-/blob/main/Ressources/cours-python.pdf)
