import re
# init le dictionnnaire
di = dict()
# ouvrir le fichier dans la variable file
with open("paragraphe.txt","r") as file:
    # mettre le paragraphe dans la variable para
    para = file.read()
    # diviser la chaine en liste de mots sans (' ',',','.','"')
    parts = re.split(" |\.|\,|\"", para)
    # on parcour les mots
    for mot in parts:
        if mot!="":
            if mot in di:
                di[mot]+=1
            else:
                di[mot]=1

print(di)


