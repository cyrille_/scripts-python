import re
from collections import Counter
fichier = "paragraphe.txt"
with open(fichier) as f:
    contenu = f.read()    
    s2 = contenu.lower().split(' ')

    count = {}
    for word in s2:
        if word not in count:
            count[word] = 1
        else:
            count[word] += 1
 
    result = []
    for k in count:
        if count[k] > 1:
            result.append(k)
            print(Counter(re.findall(k, contenu.lower())))
    print (result)