import paramiko
# charger fichier pem
k = paramiko.RSAKey.from_private_key_file("devops2021.pem")
# ouvrir transport client serveur
transport = paramiko.Transport(("devops2021.cefim-formation.org",22))
# connection auth
transport.connect(None,"admin",pkey = k)
print("connected")
#lancer sftp
sftp = paramiko.SFTPClient.from_transport(transport)
print(sftp.listdir())
# sftp.put("logo-python.png","logo-python.png")
sftp.get("logo-python.png","test-img.png")