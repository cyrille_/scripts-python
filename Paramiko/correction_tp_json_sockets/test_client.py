from unittest import TestCase
import unittest
from client import dict2json

class TestDict2Json(TestCase):

    def testDict2Json(self):
        #inputs
        d = {
            "name":"adel"
        }
        # call to function dict2json
        output = dict2json(d)
        ex_result= '{"name": "adel"}'
        self.assertEqual(output,ex_result)


# launch tests
unittest.main()
