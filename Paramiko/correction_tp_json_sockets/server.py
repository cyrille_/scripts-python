import socket
import json
server = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
server.bind(("",19200))
server.listen(3)
print("socket is listening... to port 19200")

while True:
    client,info = server.accept()
    print(f"Got connexion from {info[0]}")
    msg_from_client = client.recv(1024)
    str_json = msg_from_client.decode("UTF-8")
    with open("server.json","w", encoding='utf8') as jFile:
        jFile.write(str_json)
    di=json.loads(str_json)
    print(di,type(di))
    client.send(b"ok")
    client.close()
    break
