import socket
import json

def dict2json(_dict={}):
    return json.dumps(_dict,ensure_ascii=False)


client = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
client.connect(("127.0.0.1",19200))
d={
    "prénom":"Sarah",
    "nom":"De loor",
    "age":29,
    "city":"Paris"
}
str_json = dict2json(d)
client.send(str_json.encode("UTF-8"))
print(client.recv(1024).decode("UTF-8"))
client.close()
