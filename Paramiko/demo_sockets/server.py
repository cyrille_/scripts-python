import socket

server = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
server.bind(("",12900))
server.listen(4)

print("Le serveur écoute sur le port 12900")

cnx_client,infos_cnx= server.accept()

msg = b""

while msg!=b"fin":
    msg = cnx_client.recv(1024)
    print(msg.decode())
    cnx_client.send(b"5 sur 5")

print("End connection!")
cnx_client.close()
server.close()

