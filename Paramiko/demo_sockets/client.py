import socket

client = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
client.connect(("localhost",12900))
print("Connection established!")

msg = b""
while msg!=b"fin":
    msg = input("> ")
    client.send(msg.encode())
    msg_recv = client.recv(1024)
    print(msg_recv.decode())

print("End connection!")
client.close()



