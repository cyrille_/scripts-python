import re
import os

def isFormatUS(_str):
    pattern = "((0|1)?\d)-((0|1|2|3)?\d)-((19|20)\d\d)"
    compilePattern = re.compile(pattern)
    return compilePattern.search(_str) != None

def formatUE(_str):
    pattern = "((0|1)?\d)-((0|1|2|3)?\d)-((19|20)\d\d)"
    compilePattern = re.compile(pattern)
    gr = compilePattern.search(_str)
    return gr.group(3)+"-"+gr.group(1)+"-"+gr.group(5)

def convert(foldername):
    for filename in os.listdir(foldername):
        if isFormatUS(filename):
            ex = filename.split('.')[-1]
            os.rename(foldername+"/"+filename, foldername+"/"+formatUE(filename)+"."+ex)

for folder,subfolders,filenames in os.walk("espacetravaill/"):
    convert(folder)