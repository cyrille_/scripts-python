import paramiko

def sftp_connection(pem="devops2021.pem",host="devops2021.cefim-formation.org",username="admin"):
    # charger fichier pem
    k = paramiko.RSAKey.from_private_key_file(pem)
    # ouvrir transport client serveur
    transport = paramiko.Transport((host,22))
    # connection auth
    transport.connect(None,username,pkey = k)
    print("connected")
    #lancer sftp
    return paramiko.SFTPClient.from_transport(transport)
    #print(sftp.listdir())
    # sftp.put("logo-python.png","logo-python.png")
    #sftp.get("logo-python.png","test-img.png")

sftp = sftp_connection()
print(sftp.getcwd())
sftp.chdir("adel")
print(sftp.getcwd())