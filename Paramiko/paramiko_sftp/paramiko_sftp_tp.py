import re,os
from paramiko_sftp import sftp_connection
def isFormat(_str):
    pattern = "([a-zA-Z]+)-(\d{4}).png"
    # _str= "sdfsdfFFF-3211.png"
    pattern_compile = re.compile(pattern)
    return pattern_compile.search(_str)!=None

# connexion 
sftp=sftp_connection()
_dir = "devops"
sftp.mkdir(_dir)

for file_name in os.listdir("exemple"):
    if(isFormat(file_name)):
        print("uploading "+file_name+" ...")
        sftp.put("exemple/"+file_name,_dir+"/"+file_name)

