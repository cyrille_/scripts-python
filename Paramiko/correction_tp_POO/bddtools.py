import sqlite3 as sql

class BddTools:
    connexion = None
    cursor = None

    def __init__(self,database_name):
        BddTools.connexion = sql.connect(database_name) 
        BddTools.cursor = BddTools.connexion.cursor()

    def insertInto(self,sql):
        BddTools.cursor.execute(sql)
        BddTools.connexion.commit()

    def select(self,sql):
        return BddTools.cursor.execute(sql)

    def close(self):
        BddTools.connexion.close()
        BddTools.connexion = None
