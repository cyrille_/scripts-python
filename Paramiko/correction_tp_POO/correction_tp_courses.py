class Courses:
    TVA = 5.5
    def __init__(self,liste=[]):
        self.liste = liste # liste des courses
        self.name = ""

    def setListe(self,liste):
        self.liste = liste

    def calculer_montant_ttc(self):
        somme = 0
        for item in self.liste:
            somme += item[1]        
        prix_ttc = somme*(1+(Courses.TVA/100))
        return prix_ttc


c = Courses() # creation,instaciation

liste = [('pomme',2),('banane',3),('poire',4)]
c.setListe(liste)
print(c.calculer_montant_ttc())

liste2 = [('pomme',2),('poire',4)]
c.setListe(liste2)
print(c.calculer_montant_ttc())



