from shh_connection import *

# la connexion avec le serveur
ssh_connect()
# les cmds
cmd("mkdir devops_j4")
cmd("touch devops_j4/main.py")
cmd("echo \"print('hello world')\" >> devops_j4/main.py")
cmd("python3 devops_j4/main.py")
# close
close()