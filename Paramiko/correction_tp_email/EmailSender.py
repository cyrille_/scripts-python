import re
import smtplib
from password import EMAIL,PASS
class EmailSender:
    
    def __init__(self,subject="test",msg="salut",_from="adel@codingx.tech",to=[]):
        self.subject=subject
        self.msg =msg
        self._from=_from
        self.to=to
        #  creation instance mailserver server avec port
        self.mailer = smtplib.SMTP_SSL("smtp.hostinger.com",465)
        # presentation au server
        self.mailer.ehlo()
        self.mailer.login(EMAIL,PASS)
    
    def loadEmails(self,filename):
        with open(filename,"r") as file:
            pattern = "\w+@[1-9a-zA-Z\-]+\.[a-zA-Z]{1,5}"
            compilePattern = re.compile(pattern)
            for line in file.readlines():
                match = compilePattern.search(line)
                if(match!=None):
                    self.to.append(match.group(0))            

    def setMsg(self,msg):
        self.msg = msg
    
    def send(self):
        for email in self.to:            
            self.mailer.sendmail(self._from,email,self.msg)
            print("mail sent :) to:",email)