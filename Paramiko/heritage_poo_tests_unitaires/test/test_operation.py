from unittest import TestCase
import unittest
from operation import add, multi,majListe

class TestOperation(TestCase):

    def test_majListe(self):
        # inputs
        _str = "salut toi"
        # outputs
        liste = majListe(_str)
        liste_ex = ["SALUT","TOI"]
        self.assertEqual(liste,liste_ex)

    def test_add(self):
        # inputs
        a = 5
        b = 10
        # output
        resultat = add(a,b)
        resultat_ex = 15
        self.assertEqual(resultat,resultat_ex)


    def test_multi(self):
        # inputs
        a = 5
        b = 6
        # output
        resultat = multi(a,b)
        resultat_ex = 30
        self.assertEqual(resultat,resultat_ex)

unittest.main()
