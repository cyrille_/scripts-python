# ** Héritage **
# ** Encapsulation **
# (Abstraction) ?
# (Polymorphisme) ?

class Personne:
    def __init__(self,firstname,lastname,age):
        self.firstname = firstname
        self.lastname = lastname
        self.age=age

    def afficher(self):# prototype||signature
        return self.firstname+","+self.lastname+","+str(self.age)

class Client(Personne):
    def __init__(self,firstname,lastname,age,mat_client):
        Personne.__init__(self,firstname,lastname,age)
        self.mat_client = mat_client
    
    def afficher(self):
        return Personne.afficher(self)+","+str(self.mat_client)

class Vendeur(Personne):
    def __init__(self,firstname,lastname,age,nbrProduit):
        Personne.__init__(self,firstname,lastname,age)
        self.nbrProduit = nbrProduit      

client1 = Client("Steve","De Loor",32,321321654)
v1 = Vendeur("Asel","Dsdzfsf",32,321321654)

print(client1.afficher(),type(client1))

