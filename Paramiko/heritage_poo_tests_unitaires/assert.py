def pourcetage(score,total):
    res = None
    try:
        assert total>0,"Total doit etre positif"
        assert score >= 0,"Score > = 0"
        assert score <= total, "total > score"
        res = score / total * 100
    except Exception as e:
        res = e
    
    return res

print(pourcetage(5,9))