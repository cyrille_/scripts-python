import paramiko

client = None

def ssh_connect():
    key = paramiko.RSAKey.from_private_key_file("devops2021.pem")
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        print("Connecting...")
        client.connect(hostname="devops2021.cefim-formation.org",username="admin",pkey=key)
        print("Connected")
    except paramiko.SSHException as e:
        print(e)

def cmd(_cmd):
    stdin,stdout,stderr=client.exec_command(_cmd)
    print("****Results****")
    for line in stdout:
        print(line)
    print("****Errors****")    
    for line in stderr:
        print(line)        

def close():
    if(client!=None):
        client.close()





